<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    public function getEditProfile(Request $request)
    {
    	$user = User::where('memberId', '=', $request->input('memberId'))->first();

    	$data['user'][] = [
        	'name' 			=> $user->name,
        	'whatsapp' 		=> $user->whatsapp,
        	'address' 		=> $user->address,
        	'district'		=> $user->district,
        	'city' 			=> $user->city,
        	'province' 		=> $user->province,
        	'postalCode'	=> $user->postal_code
        ];

        return view('editProfile', compact('data'));
    }

    public function updateProfile(Request $request)
    {
    	$user = User::where('memberId', '=', $request->input('memberId'))->first();
    	$user->update([
        	'address' 	=> $request->input('address'),
        	'city' 		=> $request->input('city'),
        	'district' 	=> $request->input('district'),
        	'province' 	=> $request->input('province'),
    	]);
    	
    	return redirect()->route('login');
    }
}
