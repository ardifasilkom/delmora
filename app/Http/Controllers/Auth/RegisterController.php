<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'          => 'required|string|max:255',
            'province'      => 'required|string|max:255',
            'city'          => 'required|string|max:255',
            'district'      => 'required|string|max:255',
            'address'       => 'required|string|max:255',
            'whatsapp'      => 'required|string|max:12|unique:users',
            'password'      => 'required|string|min:6|confirmed',
            'postalCode'    => 'string|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // put the first alphabet name and the year
        $name = substr($data['name'],0,2);
        $year = substr(date("Y"), 2);

        return User::create([
            'name'          => $data['name'],
            'whatsapp'      => $data['whatsapp'],
            'address'       => $data['address'],
            'province'      => $data['province'],
            'city'          => $data['city'],
            'district'      => $data['district'],
            'postal_code'   => $data['postalCode'],
            'memberId'  => $name . date("m") . $year . str_random(2) . substr($data['whatsapp'], strlen($data['whatsapp']) - 3),
            'password' => Hash::make($data['password']),

        ]);
    }
}
