<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class SearchController extends Controller
{
    public function searchMember(Request $request)
    {
    	$key    = $request->get('memberId');
    	$users  = User::where('memberId', 'like', "%{$key}%")->get();

    	return view('result', compact('users'));
    }
}
