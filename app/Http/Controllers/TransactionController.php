<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Transaction;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
	public function appliedMember(Request $request)
    {
    	$key    = $request->get('memberId');
    	$users  = User::where('memberId', 'like', "%{$key}%")->first();

        $data['transaction'][] = [
            'memberId'      => $users->memberId,
            'name'          => $users->name,
            'whatsapp'      => $users->whatsapp,
            'address'       => $users->address,
            'district'      => $users->district,
            'city'          => $users->city,
            'province'      => $users->province,
            'courier'       => $users->courier,
            'note'          => $users->note,
            'postalCode'    => $users->postal_code,
        ];

    	return view('generateTransaction', compact('data'));
    }

    public function submit(Request $request)
    {
    	$rules = [
            'courier'		=> 'required',
        ];

        $validation = Validator::make($request->all(), $rules);
        $user = User::where('memberId', '=', $request->input('memberId'))->first();
        

        $transaction = Transaction::create([
        	'user_id'      => $user->id,
        	'courier'		=> $request->input('courier'),
        ]);

        $data['transaction'][] = [
        	'name'          => $request->input('name'),
        	'whatsapp' 	    => $request->input('whatsapp'),
        	'address' 	    => $request->input('address'),
        	'city' 		    => $request->input('city'),
        	'province' 	    => $request->input('province'),
        	'courier' 	    => $request->input('courier'),
            'note'          => $request->input('note'),
            'postalCode'    => $request->input('postalCode'),
        ];

        return view('printPackingSlip', compact('data'));
    }

    public function editTransaction(Request $request)
    {
        $user = User::where('memberId', '=', $request->input('memberId'))->first();

        $data['user'][] = [
            'name'          => $user->name,
            'whatsapp'      => $user->whatsapp,
            'address'       => $user->address,
            'district'      => $user->district,
            'city'          => $user->city,
            'province'      => $user->province,
            'postalCode'    => $user->postal_code,
            'memberId'      => $user->memberId,
        ];

        return view('editTransaction', compact('data'));
    }

    public function saveEditedTransaction(Request $request)
    {

        $user = User::where('memberId', '=', $request->input('memberId'))->first();
        $data['transaction'][] = [
            'name'          => $user->name,
            'whatsapp'      => $user->whatsapp,
            'address'       => $user->address,
            'district'      => $user->district,
            'city'          => $user->city,
            'province'      => $user->province,
            'postalCode'    => $user->postal_code
        ];

        // return redirect('/transaction/create')->with( ['data' => $data] );
        return back()->withInput();
    }

}
