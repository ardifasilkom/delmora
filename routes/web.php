<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/search', function(){
	return view('search');
});
Route::get('/transaction/create', function(){
	return view('generateTransaction');
});

Route::post('/profile/edit', 'ProfileController@getEditProfile');
Route::post('/profile/edit/submit', 'ProfileController@updateProfile');
Route::post('search', 'SearchController@searchMember');
Route::post('/transaction/create', 'TransactionController@appliedMember');
Route::post('/transaction/submit', 'TransactionController@submit');
Route::post('/transaction/edit', 'TransactionController@editTransaction');
Route::post('/transaction/edit/save', 'TransactionController@saveEditedTransaction');

