@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div id="cardCol" class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Buat Transaksi') }}</div>

                <div class="card-body">
                        @foreach ($data['transaction'] as $users)

                        <form method="POST" action="{{ url('/transaction/submit') }}">
                            @csrf

                            <input type="hidden" name="memberId" id="memberId" value="{{ $users['memberId'] }}">

                             <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama Penerima') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $users['name'] }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="whatsapp" class="col-md-4 col-form-label text-md-right">{{ __('Nomor Penerima') }}</label>

                                <div class="col-md-6">
                                    <input id="whatsapp" type="text" class="form-control{{ $errors->has('whatsapp') ? ' is-invalid' : '' }}" name="whatsapp" value="{{ $users['whatsapp'] }}" required autofocus>

                                    @if ($errors->has('whatsapp'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('whatsapp') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Alamat Penerima') }}</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ $users['address'] }}" required autofocus>

                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="district" class="col-md-4 col-form-label text-md-right">{{ __('Kecamatan') }}</label>

                                <div class="col-md-6">
                                    <input id="district" type="text" class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}" name="district" value="{{ $users['district'] }}" required autofocus>

                                    @if ($errors->has('district'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('district') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('Kota') }}</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ $users['city'] }}" required autofocus>

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="province" class="col-md-4 col-form-label text-md-right">{{ __('Provinsi') }}</label>

                                <div class="col-md-6">
                                    <input id="province" type="text" class="form-control{{ $errors->has('province') ? ' is-invalid' : '' }}" name="province" value="{{ $users['province'] }}" required autofocus>

                                    @if ($errors->has('province'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('province') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="postalCode" class="col-md-4 col-form-label text-md-right">{{ __('Kode Pos') }}</label>

                                <div class="col-md-6">
                                    <input id="postalCode" type="text" class="form-control{{ $errors->has('postalCode') ? ' is-invalid' : '' }}" name="postalCode" value="{{ $users['postalCode'] }}" required autofocus>

                                    @if ($errors->has('postalCode'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('postalCode') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="courier" class="col-md-4 col-form-label text-md-right">{{ __('Kurir Pengiriman') }}</label>

                                <div class="col-md-6">
                                    <input id="courier" type="text" class="form-control{{ $errors->has('courier') ? ' is-invalid' : '' }}" name="courier" value="{{ old('courier') }}" required autofocus>

                                    @if ($errors->has('courier'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('courier') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="note" class="col-md-4 col-form-label text-md-right">{{ __('Catatan Pengiriman') }}</label>

                                <div class="col-md-6">
                                    <input id="note" type="text" class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" value="{{ old('note') }}">

                                    @if ($errors->has('note'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('note') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4 buttonDiv">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Buat Surat Jalan') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                        @endforeach
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
