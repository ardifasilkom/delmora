@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div id="cardCol" class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Surat Jalan') }}</div>
                <div class="card-body">
                     @foreach($data['transaction'] as $transaction)
                     <div class="row">
                         <div class="col-xs-2 print">
                             <b>To :</b>
                         </div>
                         <div class="col-md-10">
                            <b>{{ $transaction['name'] }}</b>
                         </div>
                         <div class="col-md-12">
                             <b>{{ $transaction['address'] }}</b>
                         </div>
                         <div class="col-md-12">
                             <b>{{ $transaction['city'] }}, {{ $transaction['province'] }} - {{ $transaction['postalCode'] }}</b>
                         </div>
                     </div>
                     <div class="row" style="margin-top: 10px;">
                         <div class="col-xs-2 print">
                             <b>HP :</b>
                         </div>
                         <div cclass="col-md-6" style="margin-left: 10px;">
                             <b>{{ $transaction['whatsapp'] }}</b>
                         </div>
                         <div cclass="col-md-6" style="margin-left: 100px;">
                             <b>Catatan: {{ $transaction['note'] }}</b>
                         </div> 
                     </div>
                     
                     <div class="row" style="margin-top: 10px;">
                         <div class="col-xs-2 print">
                             <b>Sip :</b>
                         </div>
                         <div cclass="col-md-6" style="margin-left: 10px;">
                             <img src="/bw-logo.png" style="width: 150px;">
                         </div>
                         <div cclass="col-md-6" style="margin-left: 80px; text-align: center; width: 40%; border:2px solid black;">
                            <h2 style="margin-bottom: 0px;"> {{ $transaction['courier'] }} </h2>
                         </div>
                     </div>
                     <div class="row" style="margin-top: 15px;">
                         <div class="col-md-6" style="padding-left: 0px;">
                            <hr style="height: 2px; width: 380px;">
                         </div>
                         <div class="col-md-6" style="padding-left: 0px; text-align: right;">
                            <p>Thanks for Shopping with us</p>
                         </div>
                     </div>
                    @endforeach
                </div>
                <div class="buttonDiv hidden-print">
                    <button class="btn btn-primary buttonDiv hidden-print" onclick="cetak()">Cetak</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function cetak() {
    window.print();
}
</script>

@endsection
