@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div id="cardCol" class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body home">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h3 id="congrats">Selamat!</h3>
                    <p id="explainer">Anda telah terdaftar sebagai member Delmora</p>
                    <div class="container id-card">
                      <img src="/id-card.png" style="width: 100%">
                      <div class="title">ID Member</div>
                      <div class="member"><h3>{{ Auth::user()->memberId }}</h3></div>
                      <div class="name">{{ Auth::user()->name }}</div>
                      <div class="whatsapp">{{ Auth::user()->whatsapp }}</div>
                      <div class="address">{{ Auth::user()->city }}, {{ Auth::user()->province }}</div>
                    </div>
                    <div class="row">
                      <div class="col-xs-4 edit-button">
                        <form class="button-edit" method="POST" align="right" action="{{ url('/profile/edit') }}">
                          @csrf
                          <input type="hidden" name="memberId" id="memberId" value="{{ Auth::user()->memberId }}">
                          <button type="submit" class="btn btn-primary edit-profil">
                            {{ __('Ubah Data') }}
                          </button>
                        </form>
                      </div>
                      <div class="col-xs-4 order-button">
                        <form class="button-order">
                          <a type="submit"><input class="order" type="button" src="/order.png" onclick="randomlink()"></a>
                        </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        <!--
        /*
        Random link button- By JavaScript Kit (http://javascriptkit.com)
        Over 300+ free scripts!
        This credit MUST stay intact for use
        */
        
        //specify random links below. You can have as many as you want
        var randomlinks=new Array()
        
        randomlinks[0]="https://wa.me/6287785281413?text=Halo%20kak!%20Saya%20mau%20pesan%20😊"
        randomlinks[1]="https://wa.me/6287889940228?text=Halo%20kak!%20Saya%20mau%20pesan%20😊"
        randomlinks[2]="https://wa.me/6287889928707?text=Halo%20kak!%20Saya%20mau%20pesan%20😊"
        randomlinks[3]="https://wa.me/6285920729776?text=Halo%20kak!%20Saya%20mau%20pesan%20😊"
        randomlinks[4]="https://wa.me/6287823143708?text=Halo%20kak!%20Saya%20mau%20pesan%20😊"
        randomlinks[5]="https://wa.me/6283186649451?text=Halo%20kak!%20Saya%20mau%20pesan%20😊"
        
        function randomlink(){
        window.location=randomlinks[Math.floor(Math.random()*randomlinks.length)]
        }
        //-->
    </script>
@endsection
