<!DOCTYPE html>
<html lang="en">
  <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Delmora Fashion Store</title>

    <meta name="description" content="The B2B Personalized Fashion platform">
    <meta name="author" content="Delmora store">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="icon" href="icon.png">
  </head>
  
  <body>
    <div class="container-fluid">
	<div class="row">
		<div class="col-sm-12" style="background-color:#f8f8f8;">
			<div class="container" id="homepage" style="vertical-align: middle;margin-top:20%;">
				<div class="image-container" id="logo">
					<img class="img-responsive vertival-center" src="logo.png" alt="Delmora" width="75%" style="display:block;margin:auto;margin-top: -10%;
">
				</div>
					<h2 style="font-family: 'Quicksand', sans-serif;color:#ff235c;font-size:25px; font-weight: bold; text-align: center;margin-top: 20px;">Halo Kak!</h2>
					<p style="font-family: 'Quicksand', sans-serif;color:#333333;font-size:90%; font-weight: bold; text-align: center;">Selamat datang di Delmora Store!<br>
Silahkan tekan tombol dibawah ini untuk melakukan pemesanan melalui admin kami.<br> Selamat berbelanja, Kakak!</p>
                </div>

				<div class="image-container" id="background">
					<img class="img-responsive vertival-center" src="background.png" alt="Delmora" width="85%" style="display:block;margin:auto;margin-top: 35px">
				</div>
					<form method="post">
                        <p style="text-align:center;"><input type="button" name="B1" class="myButton" id="order" value="Pesan Sekarang" onclick="randomlink()"></p> 
                    </form>
				

			</div>
		
		<div class="col-md-12" style="background-color:#353535;padding-top: 20px;">
			<div class="row">
				<div class="col-md-6">
					<address style="color:white;font-family: 'Quicksand', sans-serif;">
						 <strong>Delmora Store</strong><br />Pusat Grosir Tanah Abang,<br /> Jakarta.<br />
					</address>
				</div>
				<div class="col-md-6">
					<p style="color:white;font-family: 'Quicksand', sans-serif;">: delmorafashionstore<img class="img-responsive" src="ig.png" alt="instagram" height="20px" style="float: left;margin-right: 5px;"></p>
				</div>
			</div>
		</div>
	</div>
</div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script>
        <!--
        /*
        Random link button- By JavaScript Kit (http://javascriptkit.com)
        Over 300+ free scripts!
        This credit MUST stay intact for use
        */
        
        //specify random links below. You can have as many as you want
        var randomlinks=new Array()
        
        randomlinks[0]="https://wa.me/6282392335151?text=Halo%20kak!%20Saya%20mau%20pesan%20😊"
        randomlinks[1]="https://wa.me/6285155210212?text=Halo%20kak!%20Saya%20mau%20pesan%20😊"
        randomlinks[2]="https://wa.me/6281930133690?text=Halo%20kak!%20Saya%20mau%20pesan%20😊"
        randomlinks[3]="https://wa.me/6285692345856?text=Halo%20kak!%20Saya%20mau%20pesan%20😊"
        
        function randomlink(){
        window.location=randomlinks[Math.floor(Math.random()*randomlinks.length)]
        }
        //-->
    </script>
    
    <style>
    
    @media only screen and (min-width: 1024px) {
    .image-container {
        display:block;margin:auto;
    }
    #background
    {
        width: 500px;
    }
    #logo
    {
        width:320px;
    }
    #homepage
    {
        style="vertical-align: middle;margin-top: 7%;"
    }
}
    	.myButton {
        	-moz-box-shadow:inset 0px 1px 0px 0px #cccccc;
        	-webkit-box-shadow:inset 0px 1px 0px 0px #cccccc;
        	box-shadow:inset 0px 1px 0px 0px #cccccc;
        	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ff235c), color-stop(1, #fb9e25));
        	background:-moz-linear-gradient(top, #ff235c 5%, #da2e5b 100%);
        	background:-webkit-linear-gradient(top, #ff235c 5%, #da2e5b 100%);
        	background:-o-linear-gradient(top, #ff235c 5%, #da2e5b 100%);
        	background:-ms-linear-gradient(top, #ff235c 5%, #da2e5b 100%);
        	background:linear-gradient(to bottom, #ff235c 5%, #da2e5b 100%);
        	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25',GradientType=0);
        	background-color:#ff235c;
        	-moz-border-radius:40px;
        	-webkit-border-radius:40px;
        	border-radius:40px;
        	border:1px solid #cccccc;
        	display:inline-block;
        	cursor:pointer;
        	color:#ffffff;
        	font-family: 'Quicksand', sans-serif;
        	font-size:21px;
        	padding:11px 60px;
        	text-decoration:none;
        	text-shadow:0px 1px 0px #cccccc;
        	margin-top: 20px;
        }
        .myButton:hover {
        	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #b93d5e), color-stop(1, #a92a4b));
        	background:-moz-linear-gradient(top, #b93d5e 5%, #a92a4b 100%);
        	background:-webkit-linear-gradient(top, #b93d5e 5%, #a92a4b 100%);
        	background:-o-linear-gradient(top, #b93d5e 5%, #a92a4b 100%);
        	background:-ms-linear-gradient(top, #b93d5e 5%, #a92a4b 100%);
        	background:linear-gradient(to bottom, #b93d5e 5%, #a92a4b 100%);
        	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#b93d5e', endColorstr='#a92a4b',GradientType=0);
        	background-color:#b93d5e;
        }
        .myButton:active {
        	position:relative;
        	top:1px;
        }

    </style>

  </body>
</html>