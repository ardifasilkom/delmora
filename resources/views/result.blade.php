@extends('layouts.app')

@section('content')
<link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
<div class="container">
    <div class="row justify-content-center">
        <div id="cardCol" class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Hasil Pencarian') }}</div>

                    <div class="card-body">
                            @foreach ($users as $users)
                            <div class="row search-result">
                                <div class="row data">
                                    <div class="col-xs-8 data">
                                        <h4 id="memberId">{{ $users->memberId }}</h4>
                                        <h5 id>{{ $users->name }}</h5>
                                        <p>{{ $users->city }}, {{ $users->province }}</p>
                                    </div>
                                </div>
                                <div class="row button">
                                        <form method="POST" action="{{ url('/transaction/create') }}">
                                            @csrf
                                            <input type="hidden" name="memberId" id="memberId" value="{{ $users->memberId }}">
                                            <div class="choose-button">
                                                <button type="submit" class="btn btn-primary choose">
                                                    {{ __('Pilih') }}
                                                </button>  
                                            </div>
                                        </form>
                                </div>
                            </div>
                            @endforeach 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
