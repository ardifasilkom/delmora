@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div id="cardCol" class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Search Member') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('/search') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="memberId" class="col-md-4 col-form-label text-md-right">{{ __('ID Member') }}</label>

                            <div class="col-md-6 search-box">
                                <input id="memberId" type="text" class="form-control{{ $errors->has('memberId') ? ' is-invalid' : '' }}" name="memberId" value="{{ old('memberId') }}" required autofocus>

                                @if ($errors->has('firstName'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('memberId') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 buttonDiv search">
                                <button type="submit" class="btn btn-primary search">
                                    {{ __('Search') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
